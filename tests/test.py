import unittest
import io
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import index
import base64

class TestAPI(unittest.TestCase):
    def setUp(self):
        self.app = index.app.test_client()
    def check(self, status_code, expected_code, text_info):
        try:
            self.assertEqual(status_code, expected_code)
            print('\033[0;32m \u2714 '+ text_info + '\033[0m')
        except AssertionError:
            print('\033[0;31m \u2718 '+ text_info + '\033[0m')
            raise

    def test_index(self):
        result = self.app.get('/')
        self.check(result.status_code, 200, "Home page successfully created.")

    def test_demo_get(self):
        result = self.app.get('/demo')
        self.check(result.status_code, 200, "Demov1 page successfully created.")

    def test_demo_generate_fake_image(self):
        file_segmap = "./tests/images/ADE_train_00003129.png"
        file_style = "./tests/images/ADE_train_00003129.jpg"
        data = {
            'file_segmap': (open(file_segmap, 'rb'), file_segmap),
            'file_style': (open(file_style, 'rb'), file_style)
        }
        result = self.app.post('/demo', data=data)
        self.check(result.status_code, 200, "(Demov1) Synthesized image successfully created.")
        self.check(result.content_type, 'image/PNG', "(Demov1) Synthesized image successfully returned.")

    def test_demo_redirect(self):
        file_segmap = "./tests/images/ADE_train_00003129.png"
        file_style = "./tests/images/ADE_train_00003129.jpg"

        result = self.app.post('/demo')
        self.check(result.status_code, 302, "(Demov1) No files.")

        data = {
            'file_style': (open(file_style, 'rb'), file_style)
        }
        result = self.app.post('/demo', data=data)
        self.check(result.status_code, 302, "(Demov1) No segmentation map.")

        data = {
            'file_segmap': (open(file_segmap, 'rb'), file_segmap)
        }
        result = self.app.post('/demo', data=data)
        self.check(result.status_code, 302, "(Demov1) No style image.")

        data = {
            'file_segmap': (io.BytesIO(b''), ''),
            'file_style': (open(file_style, 'rb'), file_style)
        }
        result = self.app.post('/demo', data=data)
        self.check(result.status_code, 302, "(Demov1) Empty segmentation map.")

        data = {
            'file_segmap': (open(file_segmap, 'rb'), file_segmap),
            'file_style': (io.BytesIO(b''), '')
        }
        result = self.app.post('/demo', data=data)
        self.check(result.status_code, 302, "(Demov1) Empty style image.")

        data = {
            'file_segmap': (io.BytesIO(b'Some text data.'), 'fake-text-stream.txt'),
            'file_style': (open(file_style, 'rb'), file_style)
        }
        result = self.app.post('/demo', data=data)
        self.check(result.status_code, 302, "(Demov1) No valid segmentation map.")

        data = {
            'file_segmap': (open(file_segmap, 'rb'), file_segmap),
            'file_style': (io.BytesIO(b'Some text data.'), 'fake-text-stream.txt')
        }
        result = self.app.post('/demo', data=data)
        self.check(result.status_code, 302, "(Demov1) No valid style image.")


    def test_demov2_get(self):
        result = self.app.get('/demov2')
        self.check(result.status_code, 200, "Demov2 page successfully created.")

    def test_demov2_generate_fake_image(self):
        file_segmap = "./tests/images/canvas.png"
        file_style = "./tests/images/ADE_train_00003129.jpg"
        with open(file_segmap, "rb") as image_file:
            encoded_string = b'data:image/png;base64,'+base64.b64encode(image_file.read())

        data = {
            'file_segmap': encoded_string,
            'file_style': (open(file_style, 'rb'), file_style)
        }
        result = self.app.post('/demov2', data=data)
        # ~ print(result.content_type)
        # ~ print(result.data)
        self.check(result.status_code, 200, "(Demov2) Synthesized image successfully created.")
        self.check(result.content_type, 'image/PNG', "(Demov2) Synthesized image successfully returned.")

    def test_demov2_redirect(self):
        file_segmap = "./tests/images/canvas.png"
        file_style = "./tests/images/ADE_train_00003129.jpg"
        with open(file_segmap, "rb") as image_file:
            encoded_string = b'data:image/png;base64,'+base64.b64encode(image_file.read())

        data = {
            'file_segmap': encoded_string
        }
        result = self.app.post('/demov2', data=data)
        self.check(result.status_code, 302, "(Demov2) No style image.")

        data = {
            'file_segmap': encoded_string,
            'file_style': (io.BytesIO(b''), '')
        }
        result = self.app.post('/demov2', data=data)
        self.check(result.status_code, 302, "(Demov2) Empty style image.")

        data = {
            'file_segmap': encoded_string,
            'file_style': (io.BytesIO(b'Some text data.'), 'fake-text-stream.txt')
        }
        result = self.app.post('/demov2', data=data)
        self.check(result.status_code, 302, "(Demov2) No valid style image.")

    def test_wrong_url(self):
        result = self.app.get('/wrong_url')
        self.assertEqual(result.status_code, 404)
        self.check(result.status_code, 404, "Wrong URL detected.")


if __name__ == '__main__':
    unittest.main()

